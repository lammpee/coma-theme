define(['jquery', '../utils/Observer'], function ($, Observer) {
    var observer = new Observer(window);
    return observer;
});