define(['jquery', './Events'], function ($, Events) {

    "use strict"

    var Observer = function (wrapper) {

        Events.apply(this, arguments);

        if (wrapper) {
            this.wrapper = wrapper;
            this.$wrapper = $(this.wrapper);
        }
        var wrapperSize = {
            width: null, height: null
        };

        this.$wrapper.resize(function (e) {
            wrapperSize.width = this.$wrapper.width();
            wrapperSize.height = this.$wrapper.height();
            triggerListeners(this, 'resize', e, wrapperSize);
            triggerListeners(this, 'scroll', e);
        }.bind(this));
        this.$wrapper.scroll(function (e) {
            triggerListeners(this, 'scroll', e);
        }.bind(this));
        this.refresh();
    };
    Observer.prototype.refresh = function (listener) {
        this.$wrapper.resize();
        this.$wrapper.scroll();
    };
    function triggerListeners(scope, eventType, e, wrapperSize) {
        window.animationFrame.add(function () {
            var data = e.currentTarget.pageYOffset;
            if (eventType == 'resize') {
                data = {
                    width: wrapperSize.width,
                    height: wrapperSize.height
                };
            }
            scope.trigger(eventType, eventType, data);
        });
    }

    return Observer;

});