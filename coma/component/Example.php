<?php

namespace CoMaTheme\Component;

class Example extends \CoMa\Base\ThemeComponent
{

    /**
     * @var string
     */
    const TEMPLATE_NAME = 'Example Component';
    /**
     * @var string
     */
    const TEMPLATE_ID = 'example';
    /**
     * @var string
     */
    const TEMPLATE_PATH = 'example';

    /**
     * @return PropertyDialog
     */
    public function getPropertyDialog()
    {
        $propertyDialog = parent::defaultPropertyDialog();
        $tab = $propertyDialog->getTab();
        // CheckBox
        $tab->addCheckBox('show_post_content', 'Show Page/Post Content')->description('Ignore headline and copy property and show content from Page / Post.');
        // Image
        $tab->addMediaImageSelectField('imageId', 'Image');
        // FieldUtil Headline
        \CoMaTheme\FieldUtil::addHeadline($tab);
        // TextBox
        $tab->addEditor('copy', 'Copy');
        return $propertyDialog;
    }

}

?>