<?php

namespace CoMaTheme\Component;

class Link extends \CoMa\Base\ThemeComponent
{

    /**
     * Visible name
     * @var string
     */
    const TEMPLATE_NAME = 'Link Component';
    /**
     * Unique ID
     * @var string
     */
    const TEMPLATE_ID = 'link';
    /**
     * Absolute path to the template
     * @var string
     */
    const TEMPLATE_PATH = 'link';

    public function __construct($properties = array(), $id = null)
    {
        parent::__construct($properties, $id);
        $this->setControls(array(self::CONTROL_RANK_UP => false, self::CONTROL_RANK_DOWN => false));
    }

    /**
     * Sets properties of the components.
     * @return \CoMa\Base\PropertyDialog
     */
    public function getPropertyDialog()
    {
        $propertyDialog = parent::defaultPropertyDialog();
        $tab = $propertyDialog->getTab();

        $tab->addLink('link', 'Link');

        return $propertyDialog;
    }

}

?>