<?php

namespace CoMaTheme\Component;

class Slider extends \CoMa\Base\ThemeComponent
{

    /**
     * @var string
     */
    const TEMPLATE_NAME = 'Slider Component';
    /**
     * @var string
     */
    const TEMPLATE_ID = 'slider';
    /**
     * @var string
     */
    const TEMPLATE_PATH = 'slider';

    /**
     * @return PropertyDialog
     */
    public function getPropertyDialog()
    {
        $propertyDialog = parent::defaultPropertyDialog();
        $tab = $propertyDialog->getTab();
        \CoMaTheme\FieldUtil::addSlider($tab);
        \CoMaTheme\FieldUtil::addScrollView($tab);
        return $propertyDialog;
    }

}

?>