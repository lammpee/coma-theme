<?php

namespace CoMaTheme\Component\Media;

class Text extends \CoMa\Base\ThemeComponent
{

    /**
     * Visible name
     * @var string
     */
    const TEMPLATE_NAME = 'Text Component';
    /**
     * Unique ID
     * @var string
     */
    const TEMPLATE_ID = 'text';
    /**
     * Absolute path to the template
     * @var string
     */
    const TEMPLATE_PATH = 'media/text';

    /**
     * Sets properties of the components.
     * @return \CoMa\Base\PropertyDialog
     */
    public function getPropertyDialog()
    {
        $propertyDialog = parent::defaultPropertyDialog();

        $tab = $propertyDialog->getTab();
        \CoMaTheme\FieldUtil::addHeadline($tab);
        $tab->addEditor('copy', 'Copy');

        \CoMaTheme\FieldUtil::addScrollView($propertyDialog);

        return $propertyDialog;
    }

}

?>