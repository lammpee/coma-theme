<?php

namespace CoMaTheme\Component\Media;

class Slider extends \CoMa\Base\ThemeComponent
{

    /**
     * Visible name
     * @var string
     */
    const TEMPLATE_NAME = 'Slider Component';
    /**
     * Unique ID
     * @var string
     */
    const TEMPLATE_ID = 'slider';
    /**
     * Absolute path to the template
     * @var string
     */
    const TEMPLATE_PATH = 'media/slider';

    /**
     * Sets properties of the components.
     * @return \CoMa\Base\PropertyDialog
     */
    public function getPropertyDialog()
    {
        $propertyDialog = parent::defaultPropertyDialog();
        $tab = $propertyDialog->getTab();
        \CoMaTheme\FieldUtil::addSlider($tab);
        \CoMaTheme\FieldUtil::addScrollView($propertyDialog);
        return $propertyDialog;
    }

}

?>