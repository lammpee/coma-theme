<?php

namespace CoMaTheme\Component\Media;

class Code extends \CoMa\Base\ThemeComponent
{
    const TAB_CODE = 'code';

    const PROPERTY_CODE = 'code';
    const PROPERTY_MAX_CODE_HEIGHT = 'max_code_height';
    const PROPERTY_SHOW_EXECUTED_CODE = 'show_executed_code';
    const PROPERTY_SHOW_HEADLINE = 'show_headline';

    /**
     * Visible name
     * @var string
     */
    const TEMPLATE_NAME = 'Code Component';
    /**
     * Unique ID
     * @var string
     */
    const TEMPLATE_ID = 'code';
    /**
     * Absolute path to the template
     * @var string
     */
    const TEMPLATE_PATH = 'media/code';

    /**
     * Sets properties of the components.
     * @return \CoMa\Base\PropertyDialog
     */
    public function getPropertyDialog()
    {
        $propertyDialog = parent::defaultPropertyDialog();

        $tab = $propertyDialog->getTab();
        $tab->addCheckBox(self::PROPERTY_SHOW_HEADLINE, 'Show Headline');
        \CoMaTheme\FieldUtil::addHeadline($tab);

        $tab = $propertyDialog->addTab(self::TAB_CODE, 'Code');
        $tab->addTextField(self::PROPERTY_MAX_CODE_HEIGHT, 'Max. Code Height')->defaultValue(240);
        $tab->addCheckBox(self::PROPERTY_SHOW_EXECUTED_CODE, 'Execute Code');
        $tab->addCodeEditor(self::PROPERTY_CODE, 'Code');

        \CoMaTheme\FieldUtil::addScrollView($propertyDialog);

        return $propertyDialog;
    }

}

?>