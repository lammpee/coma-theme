<?php

namespace CoMaTheme\Component\Media;

class Image extends \CoMa\Base\ThemeComponent
{

    /**
     * Visible name
     * @var string
     */
    const TEMPLATE_NAME = 'Image Component';
    /**
     * Unique ID
     * @var string
     */
    const TEMPLATE_ID = 'image';
    /**
     * Absolute path to the template
     * @var string
     */
    const TEMPLATE_PATH = 'media/image';

    /**
     * Sets properties of the components.
     * @return \CoMa\Base\PropertyDialog
     */
    public function getPropertyDialog()
    {
        $propertyDialog = parent::defaultPropertyDialog();
        $tab = $propertyDialog->getTab();

        $tab->addDropDown('image_size', 'Image-Size', array('Post Media' => 'post-media',
            'Post Media Default Height' => 'post-media-default-height'));
        $tab->addMediaImageSelectField('image_id', 'Image');

        \CoMaTheme\FieldUtil::addScrollView($propertyDialog);
        \CoMaTheme\FieldUtil::addPicture($tab);
        \CoMaTheme\FieldUtil::addLink($tab, 'link', 'Link');

        return $propertyDialog;
    }

}

?>