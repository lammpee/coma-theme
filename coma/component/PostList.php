<?php

namespace CoMaTheme\Component;

use CoMaTheme\FieldUtil;

class PostList extends \CoMa\Base\ThemeComponent
{

    /**
     * Visible name
     * @var string
     */
    const TEMPLATE_NAME = 'Post List Component';
    /**
     * Unique ID
     * @var string
     */
    const TEMPLATE_ID = 'posts';
    /**
     * Absolute path to the template
     * @var string
     */
    const TEMPLATE_PATH = 'postList';

    /**
     * Sets properties of the components.
     * @return \CoMa\Base\PropertyDialog
     */
    public function getPropertyDialog()
    {
        $propertyDialog = parent::defaultPropertyDialog();
        $tab = $propertyDialog->getTab();
        // Categories Select
        $tab->addCategorySelect('category', 'Categories');
        // Posts Select
        $tab->addPostsSelect('posts', 'Posts');

        $tab = $propertyDialog->addTab('content', 'Content');
        \CoMaTheme\FieldUtil::addHeadline($tab);
        $tab->addEditor('copy', 'Copy');

        \CoMaTheme\FieldUtil::addFilterSort($propertyDialog);
        \CoMaTheme\FieldUtil::addScrollView($propertyDialog);

        return $propertyDialog;
    }

}

?>