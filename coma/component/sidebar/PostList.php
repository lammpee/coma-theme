<?php

namespace CoMaTheme\Component\Sidebar;

class PostList extends \CoMa\Base\ThemeComponent
{

    /**
     * Visible name
     * @var string
     */
    const TEMPLATE_NAME = 'Sidebar Post List Component';
    /**
     * Unique ID
     * @var string
     */
    const TEMPLATE_ID = 'posts';
    /**
     * Absolute path to the template
     * @var string
     */
    const TEMPLATE_PATH = 'sidebar/postList';

    /**
     * Sets properties of the components.
     * @return \CoMa\Base\PropertyDialog
     */
    public function getPropertyDialog()
    {
        $propertyDialog = parent::defaultPropertyDialog();
        $tab = $propertyDialog->getTab();
        // Categories Select
        $tab->addCategorySelect('category', 'Categories');
        // Posts Select
        $tab->addPostsSelect('posts', 'Posts');
        // Post Amount
        $tab->addTextField('postAmount', 'Post Amount')->type('number')->defaultValue('0')->description('0 for no limit.');

        $tab = $propertyDialog->addTab('content', 'Content');
        \CoMaTheme\FieldUtil::addHeadline($tab);
        $tab->addEditor('copy', 'Copy');

        return $propertyDialog;
    }

}

?>