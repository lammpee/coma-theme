<?php

namespace CoMaTheme\Component\Sidebar;

class Menu extends \CoMa\Base\ThemeComponent
{

    /**
     * Visible name
     * @var string
     */
    const TEMPLATE_NAME = 'Sidebar Menu Component';
    /**
     * Unique ID
     * @var string
     */
    const TEMPLATE_ID = 'menu';
    /**
     * Absolute path to the template
     * @var string
     */
    const TEMPLATE_PATH = 'sidebar/menu';

    /**
     * Sets properties of the components.
     * @return \CoMa\Base\PropertyDialog
     */
    public function getPropertyDialog()
    {
        $propertyDialog = parent::defaultPropertyDialog();
        $tab = $propertyDialog->addTab('content', 'Content');
        \CoMaTheme\FieldUtil::addHeadline($tab);
        $tab->addEditor('copy', 'Copy');

        $tab = $propertyDialog->getTab();
        // Menu-Position Select
        $tab->addMenuPositionSelect('menu_select', 'Menu-Select');
        $tab->addCheckBox('archive_select', 'Archive-Select');

        return $propertyDialog;
    }

}

?>