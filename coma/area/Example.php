<?php

namespace CoMaTheme\Area;

class Example extends \CoMa\Base\ThemeArea
{

    /**
     * @var string
     */
    const TEMPLATE_NAME = 'Example Area';
    /**
     * @var string
     */
    const TEMPLATE_ID = 'exampleArea';
    /**
     * @var string
     */
    const TEMPLATE_PATH = 'example';

    public function __construct()
    {
        $this->setClass(get_class($this));
    }

    /**
     * @return array
     */
    public static function getClasses()
    {
        return array('\CoMaTheme\Component\Example');
    }

}

?>