# Content-Manager Theme for Wordpress
Content-Manager Theme ist ein Beispiel Theme für Wordpress.
Dieses soll den Einstieg in das Plugin Content-Manager erleichtern.

1. [Vorbereitung](#markdown-header-1-vorbereitung)
2. [Eigenschaften](#markdown-header-eigenschaften)
    -   [Globale-Eigenschaften](#markdown-header-a-globale-eigenschaften)
    -   [Seiten-Eigenschaften](#markdown-header-b-seiten-eigenschaften)

## 1. Vorbereitung
Für die benutzung des Themes wird das Plugin Content-Manager benötigt.

[Wordpress-Content-Manager](https://bitbucket.org/lammpee/wordpress-content-manager)

### Installation

> Zum einbinden, muss das Theme unter dem angegebenen Pfad vorhanden und aktiviert sein.
> Beispiel-Pfad: **wp-content/themes/wordpress-content-manager-theme**

### Content-Mangager aktivieren
Das Plugin muss im Template aktiviert werden, dies kann in der **functions.php** gemacht werden.

>Datei: **%template%/functions.php**

    <?php
    
    /**
     * Aktiviert das Plugin in einem Theme. 
     */
    define('\CoMa\TEMPLATE', true);
    
    ?>

### Template Ordnerstruktur
Vorrausetzung an vorhandenen Ordnern im aktiven Template-Ordner.

* contentmanager
    * area
    * component
    * template
        * area
        * component
    
### Verwendung

Nachzulesen im Tutorial und Theme als Beispiel.

## 2. Eigenschaften
### a. Globale-Eigenschaften
Globale Eigenschaften können wie folgt festgelegt werden.
    
    <?php
    
    function coMaThemePageProperties($propertyDialog)
    {
        /**
         * @type PropertyDialog $propertyDialog
         */
        $tab = $propertyDialog->getTab();
        $tab->addTextField('example', 'Example');
        return $propertyDialog;
    }
    
    /**
     * Stellt die Global Eigenschaften bereit.
     * Wenn kein Template ausgewählt ist, wird default genommen.
     */
    add_filter(\CoMa\WP\Filter\GLOBAL_PROPERTIES_DIALOG, coMaThemePageProperties);
    
    ?>

### b. Seiten-Eigenschaften

    <?php

    function ($propertyDialog, $pageId)
    {
        /**
         * @type PropertyDialog $propertyDialog
         */
        switch (get_page_template_slug($pageId)) {
            /*
             * Hier können die Seiten Eigenschaten angegeben werden.
             */
            default:
                $tab = $propertyDialog->getTab();
                $tab->addTextField('example', 'Example');
                break;
        }
        return $propertyDialog;
    }
    
    /**
     * Stellt die Eigenschaften für die Seitentemplate bereit.
     * Als Key dient der slug der Template-Datei, dies ist der Dateiname.
     * Wenn kein Template ausgewählt ist, wird default genommen.
     */
    add_filter(\CoMa\WP\Filter\PAGE_PROPERTIES_DIALOG, coMaThemeGlobalProperties, 10, 2);

    ?>