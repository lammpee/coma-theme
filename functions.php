<?php
/**
 * Content-Manager functions and definitions
 *
 * @package CoMaTheme
 */

namespace CoMaTheme;

if (file_exists(\CoMa\THEME_PATH . '/coma/FieldUtil.php')) {
    include(\CoMa\THEME_PATH . '/coma/FieldUtil.php');
}

use\CoMa\Helper\Base;
const GLOBAL_PROPERTY_SHOW_AFTER_BEFORE_RENDER_CALLBACK = 'show_after_before_render_callback';
const GLOBAL_PROPERTY_COLOR_BODY_BACKGROUND = 'color_body_background';
const GLOBAL_PROPERTY_COLOR_CHROME_BAR_BACKGROUND = 'color_chrome_bar_background';
const GLOBAL_PROPERTY_POST_DATE_FORMAT = 'post_date_format';

const GLOBAL_PAGE_TAB_HEADER = 'header';
const GLOBAL_PAGE_PROPERTY_HEADER_HEADLINE_TYPE = 'header_headline_type';
const GLOBAL_PAGE_PROPERTY_HEADER_SUBLINE_TYPE = 'header_subline_type';
const GLOBAL_PAGE_PROPERTY_HEADER_HEADLINE = 'header_headline';
const GLOBAL_PAGE_PROPERTY_HEADER_SUBLINE = 'header_subline';
const GLOBAL_PAGE_PROPERTY_HEADER_MENU_POSITION_SELECT = 'header_menu_position_select';
const GLOBAL_PAGE_TAB_FOOTER = 'footer';
const GLOBAL_PAGE_PROPERTY_FOOTER_COPYRIGHT = 'footer_copyright';
const GLOBAL_PAGE_PROPERTY_FOOTER_MENU_POSITION_SELECT = 'footer_menu_position_select';
const GLOBAL_PROPERTY_HIDE_SIDEBAR = 'hide_sidebar';
const GLOBAL_PROPERTY_SHOW_SIDEBAR_ON_POST = 'show_sidebar_on_post';

const GLOBAL_TAB_TEXT = 'text';
const GLOBAL_PROPERTY_TEXT_MORE_LINK = 'text_more_link';
const GLOBAL_PROPERTY_TEXT_BACK_LINK = 'text_back_link';
const GLOBAL_PROPERTY_TEXT_NO_POSTS = 'text_no_posts';
const GLOBAL_PROPERTY_TEXT_CODE_SOURCE = 'text_code_source';
const GLOBAL_PROPERTY_TEXT_CODE_PREVIEW = 'text_code_preview';

const PAGE_PROPERTY_COLOR_BODY_BACKGROUND = 'color_body_background';
const PAGE_PROPERTY_COLOR_CHROME_BAR_BACKGROUND = 'color_chrome_bar_background';


if (defined('\CoMa\PLUGIN_NAME')) {

// development

    define('DEVELOPMENT_DEBUG', false);
    define('DEVELOPMENT_LIVERELOAD', DEVELOPMENT_DEBUG && !Base::getWPOption(\CoMa\WP\Options\DEBUG_LIVERELOAD));

    if (DEVELOPMENT_LIVERELOAD && \CoMa\Helper\Base::isPreview()) {
        add_filter('wp_footer', function () {
            /**
             * Livereload requires running grunt default task.
             */
            ?>
            <script>document.write('<script src="http://'
                    + (location.host || 'localhost').split(':')[0]
                    + ':35731/livereload.js?snipver=1"></'
                    + 'script>')</script>
            <?php
        });
    }

// #################################################
// #################################################

    add_theme_support(\CoMa\THEME_SUPPORT_NAME);

    /**
     * Menu
     */

    add_action('init', function () {
        register_nav_menus(array(
            'header-menu' => __('Header Menu'),
            'footer-menu' => __('Footer Menu')
        ));
    }, 1);


    /**
     * Thumnails
     */
    add_theme_support('post-thumbnails');

    /**
     * Sharing
     * Large Facebook-Post-Image (600px x 315px)
     */
    add_image_size('meta', 600, 315, true);

//    add_image_size('picture-lg', 180, 180, true);   // lg
//    add_image_size('picture-md', 180, 180, true);   // md
    add_image_size('picture-sm', 991, 320, true);   // sm
    add_image_size('picture-xs', 767, 248, true);   // xs
    add_image_size('picture', 479, 154, true);      // default

    add_image_size('post-stage', 992, 320, true);      // default

    add_image_size('slider-cropped', 992, 320, true);      // default
    add_image_size('slider-default', 992);      // default

    add_image_size('post-media', 992, 480, true);      // default
    add_image_size('post-media-default-height', 992);      // default

    add_image_size('image-poster', 992, 558, true);      // default
    /**
     * Content-Manager
     */

    /**
     * Stellt die Global Eigenschaften bereit.
     * Wenn kein Template ausgewählt ist, wird default genommen.
     */
    add_filter(\CoMa\WP\Filter\GLOBAL_PROPERTIES_DIALOG, function ($propertyDialog) {

        /**
         * @type \CoMa\Base\PropertyDialog $propertyDialog
         */
        $tab = $propertyDialog->getTab();

        $tab->addCheckBox(GLOBAL_PROPERTY_SHOW_AFTER_BEFORE_RENDER_CALLBACK, 'Show before/after render callback');
        $tab->addColorPicker(GLOBAL_PROPERTY_COLOR_BODY_BACKGROUND, 'Body Background');
        $tab->addColorPicker(GLOBAL_PROPERTY_COLOR_CHROME_BAR_BACKGROUND, 'Google Chrome Bar Background');
        $tab->addCheckBox(GLOBAL_PROPERTY_HIDE_SIDEBAR, 'Hide Sidebar');
        $tab->addCheckBox(GLOBAL_PROPERTY_SHOW_SIDEBAR_ON_POST, 'Show Sidebar on Post')->description('single.php (Single Template)');
        $tab->addTextField(GLOBAL_PROPERTY_POST_DATE_FORMAT, 'Post Date Format')->description(__('Example:', \CoMa\PLUGIN_NAME) . ' Y/m/d g:i:s A - 2010/11/06 12:50:48 AM - <a href="https://codex.wordpress.org/Formatting_Date_and_Time" target="_blank">' . __('Details', \CoMa\PLUGIN_NAME) . '</a>')->defaultValue('Y/m/d g:i:s A');

        $tab = $propertyDialog->addTab(GLOBAL_TAB_TEXT, 'Text');
        $tab->addTextField(GLOBAL_PROPERTY_TEXT_MORE_LINK, 'More-Link Text')->defaultValue('Read more&#8230;');
        $tab->addTextField(GLOBAL_PROPERTY_TEXT_BACK_LINK, 'Back-Link Text')->defaultValue('Back');
        $tab->addEditor(GLOBAL_PROPERTY_TEXT_NO_POSTS, 'No Posts Text')->defaultValue('No posts');
        $tab->addTextField(GLOBAL_PROPERTY_TEXT_CODE_SOURCE, 'Code-Source Text')->defaultValue('Sourcecode');
        $tab->addTextField(GLOBAL_PROPERTY_TEXT_CODE_PREVIEW, 'Code-Preview Text')->defaultValue('Sourcecode-Preview');

        FieldUtil::addHeaderFooter($propertyDialog);
        FieldUtil::addMeta($propertyDialog);

        return $propertyDialog;
    });

    /**
     * Fügt eine Kategorie-Zuordnung beim bearbeiten von Medien hinzu.
     */
    add_action('init', function () {
        register_taxonomy_for_object_type('category', 'attachment');
    });

    /**
     * Stellt die Eigenschaften für die Seitentemplate bereit.
     * Als Key dient der slug der Template-Datei, dies ist der Dateiname.
     * Wenn kein Template ausgewählt ist, wird default genommen.
     */
    add_filter(\CoMa\WP\Filter\PAGE_PROPERTIES_DIALOG, function ($propertyDialog, $pageId) {
        /**
         * @type \CoMa\Base\PropertyDialog $propertyDialog
         */
        switch (get_page_template_slug($pageId)) {
            /*
             * Hier können die Seiten Eigenschaten angegeben werden.
             */
            default:
                $tab = $propertyDialog->getTab();
                $tab->addColorPicker(PAGE_PROPERTY_COLOR_BODY_BACKGROUND, 'Body Background');
                $tab->addColorPicker(PAGE_PROPERTY_COLOR_CHROME_BAR_BACKGROUND, 'Google Chrome Bar Background');
                $tab->addCheckBox('show_sidebar', 'Show Sidebar');

                FieldUtil::addHeaderFooter($propertyDialog);
                FieldUtil::addMeta($propertyDialog);
                break;
        }
        return $propertyDialog;
    }, 10, 2);

    add_action(\CoMa\WP\Action\BEFORE_RENDER, function ($controller) {
        if (\CoMa\Helper\Base::getGlobalProperty(GLOBAL_PROPERTY_SHOW_AFTER_BEFORE_RENDER_CALLBACK)) {
            switch ($controller::TYPE) {
                case \CoMa\Helper\Base::TYPE_AREA:
                    ?>
                    <div class="before_render">before render area [<?php echo $controller->getId(); ?>]</div>
                    <?php
                    break;
                case \CoMa\Helper\Base::TYPE_COMPONENT:
                    ?>
                    <div class="before_render">before render component [<?php echo $controller->getId(); ?>]</div>
                    <?php
                    break;
                default:
                    ?>
                    <div class="before_render">before render controller [<?php echo $controller->getId(); ?>]</div>
                    <?php
                    break;
            }
        }
        return $controller;
    });
    add_action(\CoMa\WP\Action\AFTER_RENDER, function ($controller) {
        if (\CoMa\Helper\Base::getGlobalProperty(GLOBAL_PROPERTY_SHOW_AFTER_BEFORE_RENDER_CALLBACK)) {
            switch ($controller::TYPE) {
                case \CoMa\Helper\Base::TYPE_AREA:
                    ?>
                    <div class="after_render">after render area [<?php echo $controller->getId(); ?>]</div>
                    <?php
                    break;
                case \CoMa\Helper\Base::TYPE_COMPONENT:
                    ?>
                    <div class="after_render">after render component [<?php echo $controller->getId(); ?>]</div>
                    <?php
                    break;
                default:
                    ?>
                    <div class="after_render">after render controller [<?php echo $controller->getId(); ?>]</div>
                    <?php
                    break;
            }
        }
        return $controller;
    });

    class Utils
    {


        public static function getScrollViewAttributes($properties)
        {
            if ($properties['scroll_view']) {
                $config = array();
                if ($properties[FieldUtil::PROPERTY_SCROLL_VIEW_ANIMATION_START])
                    $config['animation_start'] = $properties[FieldUtil::PROPERTY_SCROLL_VIEW_ANIMATION_START];
                if ($properties[FieldUtil::PROPERTY_SCROLL_VIEW_ONLY_FROM_BOTTOM])
                    $config['only_from_bottom'] = true;
                if ($properties[FieldUtil::PROPERTY_SCROLL_VIEW_STYLE_CLASSES])
                    $config['style_classes'] = $properties[FieldUtil::PROPERTY_SCROLL_VIEW_STYLE_CLASSES];
                return ' data-scroll-view=\'' . json_encode($config) . '\'';
            }
            return;
        }

        public static function getSliderAttributes($properties)
        {
            $config = array();
            $config['arrows'] = !!$properties[\CoMaTheme\FieldUtil::PROPERTY_SLIDER_ARROWS];
            $config['dots'] = !!$properties[\CoMaTheme\FieldUtil::PROPERTY_SLIDER_DOTS];
            $config['fade'] = !!$properties[\CoMaTheme\FieldUtil::PROPERTY_SLIDER_FADE];
            $config['speed'] = $properties[\CoMaTheme\FieldUtil::PROPERTY_SLIDER_SPEED];
            $config['autoplay'] = !!$properties[\CoMaTheme\FieldUtil::PROPERTY_SLIDER_AUTOPLAY];
            $config['autoplaySpeed'] = $properties[\CoMaTheme\FieldUtil::PROPERTY_SLIDER_AUTOPLAY_SPEED];

            if ($properties['slider_autoplay'])
                $config['autoplay'] = true;
            if (count($config) > 0) {
                return ' data-slider=\'' . json_encode($config) . '\'';
            }
            return;
        }


        public static function renderMenu($name, $echo = true, $hideEmpty = false)
        {

            if (empty($name))
                return null;
            $menu = self::getMenu($name);
            if ($menu) {

                $menuItems = self::getMenuItems($menu);
                $menuList = '<ul id="menu-' . $name . '">';
                $menuList .= self::renderItems($menuItems, $name);
                $menuList .= '</ul>';

            } else {
                $menuList = 'Menu "' . $name . '" not defined.';
            }
            if ($echo) {
                echo $menuList;
            } else {
                return $menuList;
            }
        }

        private static function renderItems($menuItems)
        {

            $menuList = '';
            foreach ((array)$menuItems as $key => $item) {
                $menuItem = $item['item'];
                $title = $menuItem->title;
                $url = $menuItem->url;
                $menuList .= '<li' . ($menuItem->object_id == \CoMa\Helper\Base::getPageId() ? ' class="selected"' : '') . '>'
                    . '<a class="assetboard " data-assetboard="link" href="' . $url . '" title="' . $title . '">' . $title . '</a>'
                    . (count($item['childrens']) > 0 ? '<ul>' . self::renderItems($item['childrens']) . '</ul>' : '') . '</li>';
            }
            return $menuList;

        }

        private static function getMenuItems($name)
        {
            if (is_string($name)) {
                $menu = self::getMenu($name);
            } else {
                $menu = $name;
            }

            if (true) {
                $items = array();
                $menuItems = wp_get_nav_menu_items($menu->term_id);
                foreach ($menuItems as $menuItem) {

                    if ($menuItem->menu_item_parent) {
                        $items[$menuItem->menu_item_parent]['childrens'][$menuItem->ID] = array('item' => $menuItem, 'childrens' => array());
                    } else {
                        $items[$menuItem->ID] = array('item' => $menuItem, 'childrens' => array());
                    }

                }

                return $items;

            }

            return wp_get_nav_menu_items($menu->term_id);


        }

        private static function getMenu($name)
        {
            $menu = null;
            if (($locations = get_nav_menu_locations()) && isset($locations[$name])) {
                $menu = wp_get_nav_menu_object($locations[$name]);
            }
            return $menu;
        }

    }

}


class Picture
{
    private $attachmentId;
    private $size;
    private $title;
    private $alt;
    private $styleClasses;
    private $medias = array(
        'lg' => '(min-width: 1200px)',
        'md' => '(min-width: 992px)',
        'sm' => '(min-width: 768px)',
        'xs' => '(min-width: 480px)',
        'default' => null,
    );

    /**
     * @param int|array $attachmentId
     * @param string $size
     * @return Picture
     */
    public static function picture($attachmentId = null, $size = null)
    {
        return new Picture($attachmentId, $size);
    }

    public function __construct($attachmentId = null, $size = null)
    {
        $this->size = $size;
        if (is_array($attachmentId)) {
            $this->sources = $attachmentId;
        } else if ($attachmentId) {
            $this->attachmentId = $attachmentId;
            $this->sources = $this->getSources();
        }
    }

    /**
     * @param int $attachmentId
     * @return Picture|int
     */
    public function attachmentId($attachmentId = null)
    {
        if ($attachmentId) {
            $this->attachmentId = $attachmentId;
            return $this;
        }
        return $this->attachmentId;
    }

    /**
     * @param string $title
     * @return Picture|string
     */
    public function title($title = null)
    {
        if ($title) {
            $this->title = $title;
            return $this;
        }
        if (empty($this->title)) {
            return get_the_title($this->attachmentId);
        }

        return $this->title;
    }

    /**
     * @param string $alt
     * @return Picture|string
     */
    public function alt($alt = null)
    {
        if ($alt) {
            $this->alt = $alt;
            return $this;
        }
        return $this->alt;
    }

    /**
     * @param array $styleClasses
     * @return Picture|string
     */
    public function styleClasses($styleClasses = null)
    {
        if ($styleClasses) {
            $this->styleClasses = $styleClasses;
            return $this;
        }
        return $this->styleClasses;
    }

    /**
     * @param array $sources
     * @return array
     */
    public function sources($sources = null)
    {
        if ($sources) {
            $this->sources = $sources;
            return $sources;
        }
        return $this->sources;
    }

    /**
     * @param array $medias
     * @return array
     */
    public function medias($medias = null)
    {
        if ($medias) {
            $this->medias = $medias;
            return $medias;
        }
        return $this->medias;
    }


    public function render()
    {

        $html = '<picture' . ($this->styleClasses ? ' class="' . $this->styleClasses . '"' : '') . '>
        <!--[if IE 9]><video><![endif]-->';

        if ($this->sources) {

            foreach ($this->sources as $key => $source) {
                $html .= '<source class="' . $key . '" srcset="' . $source . '"';
                if ($this->medias[$key]) {
                    $html .= ' media="' . $this->medias[$key] . '"';
                }
                $html .= ' />';
            }

        }

        $html .= '<!--[if IE 9]></video><![endif]-->
        <img src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"' . ($this->alt != null ? ' alt="' . $this->alt . '"' : '') . ($this->title != null ? ' alt="' . $this->title . '"' : '') . ' />
    </picture>
    <script>
        var pictures = document.getElementsByTagName(\'picture\');
        window.picture.parse(pictures[pictures.length - 1]);
    </script>';

        return $html;

    }

    private function getSources()
    {
        $sources = array();
        if (wp_get_attachment_image_src($this->attachmentId, $this->size)) {
            if (has_image_size($this->size . '-lg'))
                $sources['lg'] = current(wp_get_attachment_image_src($this->attachmentId, $this->size . '-lg'));
            if (has_image_size($this->size . '-md'))
                $sources['md'] = current(wp_get_attachment_image_src($this->attachmentId, $this->size . '-md'));
            if (has_image_size($this->size . '-sm'))
                $sources['sm'] = current(wp_get_attachment_image_src($this->attachmentId, $this->size . '-sm'));
            if (has_image_size($this->size . '-xs'))
                $sources['xs'] = current(wp_get_attachment_image_src($this->attachmentId, $this->size . '-xs'));
            if (has_image_size($this->size))
                $sources['default'] = current(wp_get_attachment_image_src($this->attachmentId, $this->size));
        }
        return $sources;
    }

}

function showSidebar()
{
    $condition = false;
    $condition = $condition || \is_single() && \CoMa\Helper\Base::getProperty(GLOBAL_PROPERTY_SHOW_SIDEBAR_ON_POST);
    $condition = $condition || !\CoMa\Helper\Base::getProperty(GLOBAL_PROPERTY_HIDE_SIDEBAR);
    $condition = $condition || \CoMa\Helper\Base::getProperty(GLOBAL_PROPERTY_HIDE_SIDEBAR) && \CoMa\Helper\Base::getProperty('show_sidebar');
    $condition = $condition || \CoMa\Helper\Base::isEditMode();
    return $condition;

}


?>